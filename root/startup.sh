#! /bin/sh

ACTIVE=true
LEASES=/leases/upnp.leases
EXTERNAL_INTERFACE=${__DEFAULT_INTERFACE}
INTERNAL_INTERFACE=${__SECONDARY_INTERFACE}
TTL=3600 # 1 hour
TTL2=1800 # TTL/2

# Default IP
IP=$(ip addr show dev ${EXTERNAL_INTERFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)

if [ "${BRIDGE_MODE}" = "true" ]; then

  IPI=$(ip addr show dev ${INTERNAL_INTERFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2}\b" | head -1)
  IPE=$(ip addr show dev ${EXTERNAL_INTERFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2}\b" | head -1)
  MAC=$(ip link show dev ${EXTERNAL_INTERFACE} | grep -oE "\b([a-f0-9:]){17}*\b" | head -1)

  /sbin/ip addr del ${IPI} dev ${INTERNAL_INTERFACE}
  /sbin/ip addr del ${IPE} dev ${EXTERNAL_INTERFACE}

  /usr/sbin/brctl addbr br0
  /usr/sbin/brctl addif br0 ${INTERNAL_INTERFACE}
  /usr/sbin/brctl addif br0 ${EXTERNAL_INTERFACE}

  /sbin/ip addr add ${IPE} dev br0
  /sbin/ip link set br0 address ${MAC}
  /sbin/ip link set br0 up

  MONITOR_INTERFACE=br0                    

else

  # Create MINIUPNPD tables
  iptables -t nat    -N MINIUPNPD
  iptables -t mangle -N MINIUPNPD
  iptables -t filter -N MINIUPNPD
  iptables -t nat    -N MINIUPNPD-POSTROUTING
  iptables -t nat    -I PREROUTING  -d ${IP} -j MINIUPNPD
  iptables -t mangle -I PREROUTING  -i ${EXTERNAL_INTERFACE} -j MINIUPNPD
  iptables -t mangle -I PREROUTING  -i ${INTERNAL_INTERFACE} -j MINIUPNPD
  iptables -t filter -I FORWARD     -i ${EXTERNAL_INTERFACE} ! -o ${EXTERNAL_INTERFACE} -j MINIUPNPD
  iptables -t nat    -I POSTROUTING -o ${EXTERNAL_INTERFACE} -j MINIUPNPD-POSTROUTING
  iptables -t nat    -F MINIUPNPD
  iptables -t mangle -F MINIUPNPD
  iptables -t filter -F MINIUPNPD
  iptables -t nat    -F MINIUPNPD-POSTROUTING

  # Masquarade traffic
  iptables -t filter -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -i ${INTERNAL_INTERFACE} -j ACCEPT
  iptables -t nat    -A POSTROUTING -o ${EXTERNAL_INTERFACE} -j MASQUERADE
  iptables -t nat    -A POSTROUTING -o ${INTERNAL_INTERFACE} -j MASQUERADE

  # Internet only
  if [ "${INTERNET_ONLY}" != "false" ]; then
    iptables -t filter -A FORWARD -i ${INTERNAL_INTERFACE} -d 10.0.0.0/8 -j DROP
    iptables -t filter -A FORWARD -i ${INTERNAL_INTERFACE} -d 172.16.0.0/12 -j DROP
    iptables -t filter -A FORWARD -i ${INTERNAL_INTERFACE} -d 192.168.0.0/16 -j DROP
  fi

  # dns
  if [ "${DNS}" != "" ]; then
    echo "nameserver ${DNS}" > /etc/dnsmasq_resolv.conf
  else
    cp /etc/resolv.conf /etc/dnsmasq_resolv.conf
  fi

  update_leases() {
    cat ${LEASES} | while read line; do
      protocol=$(echo $line | cut -d':' -f1)
      port=$(echo $line | cut -d':' -f2)
      ip=$(echo $line | cut -d':' -f3)
      id=$(echo $line | cut -d':' -f6-9 | sed "s/ /_/g")
      upnpc -e ${id} -n ${IP} ${port} ${port} ${protocol} ${TTL}
    done
  }

  cat > /etc/miniupnpd.conf <<__EOF__
ext_ifname=${EXTERNAL_INTERFACE}
listening_ip=${INTERNAL_INTERFACE}
enable_natpmp=yes
enable_upnp=yes
min_lifetime=120
max_lifetime=86400
lease_file=${LEASES}
secure_mode=no
system_uptime=yes
notify_interval=60
uuid=6574C473-47BB-4C5A-9BF6-FAE7130DE229
serial=1
__EOF__
  touch ${LEASES}

  update_leases

  /usr/sbin/miniupnpd
  /usr/sbin/dnsmasq

  if [ "${NAT}" != "false" ]; then
    (touch ${LEASES} ; inotifywait --quiet --monitor ${LEASES} --event close_write | while read event; do
      #echo $event
      update_leases
    done) &
    (while ${ACTIVE}; do
      update_leases
      sleep ${TTL2} &
      wait "$!"
    done) &
  fi

  MONITOR_INTERFACE=${EXTERNAL_INTERFACE}

fi

# Replace default monitoring
iptables -D OUTPUT -j TX
iptables -D INPUT -j RX
iptables -I FORWARD -o ${MONITOR_INTERFACE} -j TX
iptables -I FORWARD -i ${MONITOR_INTERFACE} -j RX

trap "ACTIVE=false ; killall sleep dnsmasq miniupnpd upnpc; exit" TERM INT

sleep 2147483647d &
wait "$!"

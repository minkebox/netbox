FROM alpine:latest

RUN apk add dnsmasq miniupnpd miniupnpc inotify-tools ;\
    rm -rf /etc/dnsmasq.conf /etc/miniupnpd

COPY root/ /

VOLUME /etc/dnsmasq.d /leases

EXPOSE 53 53/udp

ENTRYPOINT ["/startup.sh"]
